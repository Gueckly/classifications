from Libs.Alogorithms.NiaveBayes import NiaveBayes

class MachineLearning(object):
    
    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs
        print('MachineLearning init',kwargs)

    # Naivae Bayes Algorithm
    def NiaveBayes(self):
        return NiaveBayes(**self.kwargs)

    # Another Algorithms
