from Libs.Alogorithms.Algorithm import Algorithm 
import json
import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()
import pickle
import operator
from Libs.Unicode.UnicodeSplit import UnicodeSplit

class NiaveBayes(Algorithm):
	
	def __init__(self,  *args, **kwargs):
		self.kwargs = kwargs

	def loadDataset(self):
		with open(self.kwargs['dataset']) as data:
			self.training_data = json.load(data)
		print('load dataset complete')
	
	def train(self):
		self.training()
		self.loadModel()
		self.benchmark()
		print('training completed' )
		

	def loadModel(self):
		with open(self.kwargs['model'], 'rb') as handle:
			model = pickle.load(handle)
		self.frequency 		= model['frequency'] 		
		self.prob_of_class 	= model['prob_of_class'] 	
		self.classes 		= model['classes'] 		
		self.dataset_size 	= model['dataset_size'] 
		self.sum_words 		= model['sum_words'] 		
		self.sum_dataset_size = model['sum_dataset_size']
		print('load trained model')
		
	def predict(self,sentence):
		words 		= self.cleanSentence(sentence)
		p = dict()
		for c in self.classes:
			# P(Class | X) 
			p_class_x 	= self.calP_Class_X(self.frequency,self.dataset_size,c,words)
			# P(Class)
			p_class 	= self.prob_of_class[c]/self.sum_dataset_size
			# P(X)	
			p_x 		= self.calP_X(self.sum_words,self.dataset_size,c,words)
			# print(' == ', p_class_x,p_class,p_x)
			p[c + '_x'] = p_class_x * p_class / p_x
		

		p = sorted(p.items(), key=operator.itemgetter(1),reverse=True)
		print('predict for classes',p )
		return p

	# supporting functions
	def benchmark(self):
		benchmarks = [	"how much does it cost?",
			"What time is it",
			"How much does it cost",
			"what is an embassy",
			"Where is the place to make my visa",
			"Will it cost more than 50 dollar",
			"How much will it cost me",
			"What is the mean time to do my visa",
			"How long do I need to make my visa",
			"Can you help me finding the right visa",
			"how long will it takes and How much will it costs me?"
		]

		for benchmark in benchmarks:
			print('benchmark',benchmark)
			self.predict(benchmark)

	def saveModel(self,model):
		with open(self.kwargs['model'], 'wb') as handle:
			pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

	def training(self):
		frequency,prob_of_class,classes,dataset_size,sum_words,sum_dataset_size  = self.createModel(self.training_data)
		model = dict()
		model['frequency'] 		= frequency
		model['prob_of_class'] 	= prob_of_class
		model['classes'] 		= classes 
		model['dataset_size'] 	= dataset_size
		model['sum_words'] 		= sum_words
		model['sum_dataset_size'] = sum_dataset_size
		self.saveModel(model)
		
		print('========== frequency ==========')
		print('frequency',frequency)
		print('========== prob_of_class ==========')
		print('prob_of_class',prob_of_class)
		print('========== classes ==========')
		print('classes',classes)
		print('========== dataset_size ==========')
		print('dataset_size',dataset_size)
		print('========== sum_words ==========')
		print('sum_words',sum_words)


	# sum of all frequency words in dictionary
	def sumWordofClasses(self,frequency):
		sumWords = dict()
		# frequency is store words count by class
		for c in frequency:
			for word in frequency[c]:
				if word not in sumWords: sumWords[word] = int()
				sumWords[word] += frequency[c][word]
		# print('word',sumWords)
		return sumWords

	def calTotalDataset(self,dataset_size):
		sum_dataset_size = 0
		for size in dataset_size:
			sum_dataset_size += dataset_size[size]	
		return sum_dataset_size

	
	def calP_Class_X(self,frequency,dataset_size,c,words):
		result = 1
		for word in words:
			if word in frequency[c]:
				# print(c,' frequency[c]',frequency[c][word],'dataset_size[c]',dataset_size[c])
				result *= frequency[c][word]/dataset_size[c]
			else:
				result *= 0.05 # alpha
		return result 

	def calP_X(self,sum_words,dataset_size,c,words):
		result = 1
		sum_dataset_size = self.calTotalDataset(dataset_size)
		for word in words:
			if word in sum_words:
				# print('frequency[c][word]',frequency[c][word],'dataset_size[c]',dataset_size[c])
				result *= sum_words[word]/sum_dataset_size
		return result	

	# clean sentence and return array of words
	# using nltk to stem word, example ['love','loving'] => lov
	def cleanSentence(self,sentence):
		ignore_words 	= ['?']
		words = []
		# tokenize each word in the sentence
		if self.kwargs['mode'] == 'unicode' :
			words = UnicodeSplit().unicode_split(sentence)
		else:
			words = nltk.word_tokenize(sentence)
			words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
		
		return words

	def createModel(self,training_data):
		classes 		= []
		documents 		= []
		ignore_words 	= ['?']
		frequency 		= dict()
		prob_of_class 	= dict()
		dataset_size 	= dict()
		# loop through each sentence in our training data
		for pattern in training_data['intents']:
			for intent in pattern['sentence']:
				if pattern['class'] not in dataset_size:
					dataset_size[pattern['class']] = int()
				dataset_size[pattern['class']] += 1
				# initial array frequency of class frequency['greetings']
				if pattern['class'] not in frequency:
					frequency[pattern['class']] = dict()
				# initial array frequency of class frequency['greetings']
				if pattern['class'] not in prob_of_class:
					prob_of_class[pattern['class']] = int()
				#frequency[pattern['class']][word] += 1
				prob_of_class[pattern['class']] += 1
				# clean sentence
				w = self.cleanSentence(intent)
				# count number of frequency of word
				for word in w:
					if word in frequency[pattern['class']]:
						frequency[pattern['class']][word] += 1
					else:
						frequency[pattern['class']][word] = 1
				# add to documents in our corpus
				documents.append((w, pattern['class']))
				# add to our classes list
				if pattern['class'] not in classes:
					classes.append(pattern['class'])
		sum_words 		 = self.sumWordofClasses(frequency)
		sum_dataset_size = self.calTotalDataset(dataset_size)
		return frequency,prob_of_class,classes,dataset_size,sum_words,sum_dataset_size







	
	