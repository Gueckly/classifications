
from abc import ABCMeta, abstractmethod

class Algorithm(object):
    
    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs

    @abstractmethod
    def loadDataset(self):
        pass
    
    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def loadModel(self):
        pass
        
    @abstractmethod
    def predict(self):
        pass
 